
/**
 * Write a description of class MarkovZero here.
 * 
 * @author Duke Software
 * @version 1.0
 */

//import java.util.Random;
import java.util.*;

public class MarkovOne {
    private String myText;
    private Random myRandom;
    
    public ArrayList<String> getFallovs(String key){
        ArrayList<String> result = new ArrayList<String>();
        int pos = 0;
        int index = 0;
        
        while (pos < myText.length()-1){
            index = myText.indexOf(key, pos);
            
            if (index < 0)
                break;
            
            if (index + 2 > myText.length())
                break;
                
            result.add(myText.substring(index + 1, index + 2));
            pos = index + 1;
        }
        
        return result;
       }
    
    public MarkovOne() {
        myRandom = new Random();
    }
    
    public void setRandom(int seed){
        myRandom = new Random(seed);
    }
    
    public void setTraining(String s){
        myText = s.trim();
    }
    
    public String getRandomText(int numChars){
        if (myText == null){
            return "";
        }

        StringBuilder sb = new StringBuilder();        
        
        int step = 1;
        int index = myRandom.nextInt(myText.length() - step);        
        String key = myText.substring(index, index + step);
        sb.append(key);

        for(int k=0; k < numChars-step; k++){
            ArrayList<String> follows = getFallovs(key);
            
            if (follows.size() ==0)
                break;            
            
            index =  myRandom.nextInt(follows.size());
            String next = follows.get(index);
            sb.append(next);

            key = key.substring(step) + next;
        }
        
        return sb.toString();
    }
}
