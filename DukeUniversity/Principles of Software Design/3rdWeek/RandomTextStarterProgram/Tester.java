
/**
 * Write a description of class Tester here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.*;
import edu.duke.*;

public class Tester {

    public void testGetFollows(){
        MarkovOne markov = new MarkovOne();
        String st = "this is a test yes this is a test.";
        markov.setTraining(st);        
        ArrayList<String> test = new ArrayList<String>();
        test = markov.getFallovs("e");      
        
        for (String str : test){
            System.out.println(str + " "); 
        }
    }
    
    public void testGetFollowsWithFile(){
        FileResource fr = new FileResource();
        String st = fr.asString();
        st = st.replace('\n', ' ');
        MarkovOne markov = new MarkovOne();
        markov.setTraining(st);        

        ArrayList<String> test = new ArrayList<String>();
        test = markov.getFallovs("t");      
        
        System.out.println(test.size());
    }
}
