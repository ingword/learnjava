import java.util.*;
import edu.duke.*;

public class EarthQuakeClient
{

    public EarthQuakeClient() {
        // TODO Auto-generated constructor stub
    }

    public ArrayList<QuakeEntry> filterByPhrase(ArrayList<QuakeEntry> quakeData,
    String indicateSearch,
    String namedPhrase) {
        ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
        
        for (QuakeEntry qe : quakeData){

            if (indicateSearch.equalsIgnoreCase("start")) {
                if ((qe.getInfo().startsWith(namedPhrase)))
                    answer.add(qe);
            } else 
            if (indicateSearch.equalsIgnoreCase("end")) {
                if ((qe.getInfo().endsWith(namedPhrase)))
                    answer.add(qe);
            } else 
            if (indicateSearch.equalsIgnoreCase("any")) {
                if ((qe.getInfo().contains(namedPhrase)))
                    answer.add(qe);
            }            
        }

        return answer;
    }
    
    public ArrayList<QuakeEntry> filterByDepth(ArrayList<QuakeEntry> quakeData,
    double minDepth,
    double maxDepth) {
        ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
        
        for (QuakeEntry qe : quakeData){
            if ((qe.getDepth() > minDepth) && (qe.getDepth() < maxDepth))
                answer.add(qe);
        }

        return answer;
    }
    
    public ArrayList<QuakeEntry> filterByMagnitude(ArrayList<QuakeEntry> quakeData,
    double magMin) {
        ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
        
        for (QuakeEntry qe : quakeData){
            if (qe.getMagnitude() > magMin)
                answer.add(qe);
        }

        return answer;
    }

    public ArrayList<QuakeEntry> filterByDistanceFrom(ArrayList<QuakeEntry> quakeData,
    double distMax,
    Location from) {
        ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
        
        for (QuakeEntry qe : quakeData){
           System.out.println(qe.getLocation().distanceTo(from));
            if ((qe.getLocation().distanceTo(from) / 1000) < distMax){
                answer.add(qe);
            }

        }

        return answer;
    }

    public void dumpCSV(ArrayList<QuakeEntry> list){
        System.out.println("Latitude,Longitude,Magnitude,Info");
        for(QuakeEntry qe : list){
            System.out.printf("%4.2f,%4.2f,%4.2f,%s\n",
                qe.getLocation().getLatitude(),
                qe.getLocation().getLongitude(),
                qe.getMagnitude(),
                qe.getInfo());
        }

    }

    public void bigQuakes() {
        EarthQuakeParser parser = new EarthQuakeParser();
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        String source = "data/nov20quakedatasmall.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        System.out.println("read data for "+list.size()+" quakes");

        ArrayList<QuakeEntry> bigQuake = filterByMagnitude(list, 5.0);
        
        for (QuakeEntry qe : bigQuake){
            System.out.println(qe);
        }
        System.out.println("Found " + bigQuake.size() + " quakes that match that criteria ");        
    }

    public void closeToMe(){
        EarthQuakeParser parser = new EarthQuakeParser();
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        String source = "data/nov20quakedatasmall.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        System.out.println("read data for "+list.size()+" quakes");

        // This location is Durham, NC
        //Location city = new Location(35.988, -78.907);

        // This location is Bridgeport, CA
        Location city =  new Location(38.17, -118.82);
        
        ArrayList<QuakeEntry> closeQuake = filterByDistanceFrom(list, 1000, city);
        
        for (QuakeEntry qe : closeQuake){
            System.out.println(qe);// + ' ' + qe.toString());
        }
        
        System.out.println("Found " + closeQuake.size() + " quakes that match that criteria ");
    }
    
    public void quakesOfDepth(){
        EarthQuakeParser parser = new EarthQuakeParser();
        //String source = "data/nov20quakedatasmall.atom";
        String source = "data/nov20quakedata.atom";        
       
        ArrayList<QuakeEntry> list  = parser.read(source);
        System.out.println("read data for "+list.size()+" quakes");

        ArrayList<QuakeEntry> depthQuake = filterByDepth(list, -10000.0, -8000.0);
        /*
        for (QuakeEntry qe : depthQuake){
            System.out.println(qe);// + ' ' + qe.toString());
        }*/

        System.out.println("Found " + depthQuake.size() + " quakes that match that criteria ");        
    }
    
    public void quakesByPhrase(){
        EarthQuakeParser parser = new EarthQuakeParser();
        //String source = "data/nov20quakedatasmall.atom";
        String source = "data/nov20quakedata.atom";        
        ArrayList<QuakeEntry> list  = parser.read(source);
        System.out.println("read data for "+list.size()+" quakes");

        String indicateString = "any";
        String phraseString = "Creek";
        
        ArrayList<QuakeEntry> phraseQuake = filterByPhrase(list, indicateString, phraseString);
        
        for (QuakeEntry qe : phraseQuake){
            System.out.println(qe);
        }

        System.out.println("Found " + phraseQuake.size() + " quakes that match " + phraseString + " at " + indicateString);        
    }

    public void createCSV(){
        EarthQuakeParser parser = new EarthQuakeParser();
        String source = "data/nov20quakedatasmall.atom";
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        dumpCSV(list);
        System.out.println("# quakes read: "+list.size());
    }
}
