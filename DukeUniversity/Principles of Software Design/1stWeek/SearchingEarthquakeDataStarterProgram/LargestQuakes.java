import java.util.*;

/**
 * Write a description of class LargestQuakes here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public class LargestQuakes {
    public void findLargestQuakes() {
        EarthQuakeParser parser = new EarthQuakeParser();
        String source = "data/nov20quakedata.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        
        /*
        for (QuakeEntry qe : list){
            System.out.println(qe);
        }
        */
        System.out.println("read data for "+list.size()+" quakes");
       
        //int largestIndex = indexOfLargest(list);
       
        //System.out.println("largest quake index = " + largestIndex);       
        //System.out.println("largest quakes is " + list.get(largestIndex));
        ArrayList<QuakeEntry> largestQuakes  = getLargest(list, 50);
       
        for (QuakeEntry qe : largestQuakes)
            System.out.println(qe);              
    }

    public int indexOfLargest(ArrayList<QuakeEntry> quakes){
        
        int largeIndex = 0;
        
        for (int i=0; i<quakes.size(); i++){
            if (quakes.get(i).getMagnitude() > quakes.get(largeIndex).getMagnitude())
                largeIndex = i;            
        }
                
        return largeIndex;
    }
    
    public ArrayList<QuakeEntry> getLargest(ArrayList<QuakeEntry> quakeData, 
    int howMany){
        ArrayList<QuakeEntry> copy = quakeData;
        ArrayList<QuakeEntry> ret = new ArrayList<QuakeEntry>();
        
        while (howMany > 0){
            int largeIndex;
            
            largeIndex = indexOfLargest(copy);
            ret.add(copy.get(largeIndex));
            copy.remove(largeIndex);
            howMany--;
        }
        
        return ret;
    }
}
