
/**
 * Write a description of class PhraseFilter here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PhraseFilter implements Filter{
    private String indicateSearch;
    private String phrase;
    
    public PhraseFilter(String is, String ph){
        indicateSearch = is;
        phrase = ph;
    }
    
    public boolean satisfies(QuakeEntry qe){
        if (indicateSearch.equalsIgnoreCase("start")) {
            if ((qe.getInfo().startsWith(phrase)))
                return true;
        } else 
        if (indicateSearch.equalsIgnoreCase("end")) {
            if ((qe.getInfo().endsWith(phrase)))
                return true;
        } else 
        if (indicateSearch.equalsIgnoreCase("any")) {
            if ((qe.getInfo().contains(phrase)))
                return true;
        } else
            return false;
        
        return false;
    }
    
    public String getName(){
        return "Phrase";
    }    
}
