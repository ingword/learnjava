
/**
 * Write a description of class DistanceFilter here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DistanceFilter implements Filter {
    private double distance;
    private Location location;
    
    public DistanceFilter(Location loc, double dist){
        location = loc;
        distance = dist;
    }
    
    public boolean satisfies(QuakeEntry qe){
        if ((qe.getLocation().distanceTo(location) / 1000) < distance)
            return true;
        else
            return false;
    }
    
    public String getName(){
        return "Distance";
    }
}
