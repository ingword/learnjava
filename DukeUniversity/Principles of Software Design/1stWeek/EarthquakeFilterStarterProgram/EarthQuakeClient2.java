import java.util.*;
import edu.duke.*;

public class EarthQuakeClient2
{
    public EarthQuakeClient2() {
        // TODO Auto-generated constructor stub
    }

    public ArrayList<QuakeEntry> filter(ArrayList<QuakeEntry> quakeData, Filter f) { 
        ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
        for(QuakeEntry qe : quakeData) { 
            if (f.satisfies(qe)) { 
                answer.add(qe); 
            } 
        } 
        return answer;
    } 

    public void quakesWithFilter() { 
        EarthQuakeParser parser = new EarthQuakeParser(); 
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        String source = "data/nov20quakedata.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);         
        System.out.println("read data for "+list.size()+" quakes");

        EarthQuakeClient2 eqc = new EarthQuakeClient2(); 
        
        /*
        Filter f1 = new MagnitudeFilter(4.0, 5.0);
        
        ArrayList<QuakeEntry> afterMagnitudeFilter  = eqc.filter(list, f1);
        
        for (QuakeEntry qe: afterMagnitudeFilter) { 
            System.out.println(qe);
        }

        System.out.println("after use " + f1 + " have " + afterMagnitudeFilter.size());        

        Filter f2 = new DepthFilter(-35000.0, -12000.00);
        
        ArrayList<QuakeEntry> afterDepthFilter  = eqc.filter(afterMagnitudeFilter, f2);
        
        System.out.println("after use " + f2 + " have " + afterDepthFilter.size());
        
        for (QuakeEntry qe: afterDepthFilter) { 
            System.out.println(qe);
        }
        */
       
        //maf.AddFilter(new DistanceFilter(new Location(39.7392, -104.9903), 1000));
        //maf.AddFilter(new PhraseFilter("end", "a"));       
       
       Location city = new Location(39.7392, -104.9903);
       
       Filter f1 = new DistanceFilter(city, 1000); 
       
       ArrayList<QuakeEntry> afterDistanceFilter  = eqc.filter(list, f1);
        
       for (QuakeEntry qe: afterDistanceFilter) { 
            System.out.println(qe);
       }

       System.out.println("after use " + f1 + " have " + afterDistanceFilter.size());        

       Filter f2 = new PhraseFilter("end", "a"); 
       
       ArrayList<QuakeEntry> afterPhraseFilter  = eqc.filter(afterDistanceFilter, f2);
        
       for (QuakeEntry qe: afterPhraseFilter) { 
            System.out.println(qe);
       }

       System.out.println("after use " + f2 + " have " + afterPhraseFilter.size());               
      
    }

    public void createCSV(){
        EarthQuakeParser parser = new EarthQuakeParser();
        //String source = "../data/nov20quakedata.atom";
        String source = "data/nov20quakedatasmall.atom";
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        dumpCSV(list);
        System.out.println("# quakes read: "+list.size());
    }

    public void dumpCSV(ArrayList<QuakeEntry> list){
        System.out.println("Latitude,Longitude,Magnitude,Info");
        for(QuakeEntry qe : list){
            System.out.printf("%4.2f,%4.2f,%4.2f,%s\n",
                qe.getLocation().getLatitude(),
                qe.getLocation().getLongitude(),
                qe.getMagnitude(),
                qe.getInfo());
        }
    }

    public void testMatchAllFilter(){
        EarthQuakeParser parser = new EarthQuakeParser(); 
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        String source = "data/nov20quakedatasmall.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);         
        System.out.println("read data for "+list.size()+" quakes");
        /*   
        for (QuakeEntry qe: list) { 
            System.out.println(qe);
        }
        */
        System.out.println("read of " + list.size());
        
        MatchAllFilter maf = new MatchAllFilter();
        
        maf.AddFilter(new MagnitudeFilter(0.0, 2.0));
        maf.AddFilter(new DepthFilter(-100000.0, -10000.0));
        maf.AddFilter(new PhraseFilter("any", "a"));
        
        ArrayList<QuakeEntry> matchFilters = filter(list, maf);
        
        for (QuakeEntry qe: matchFilters) { 
            System.out.println(qe);
        }
        System.out.println("Filters used are: " + maf.getName());        
        System.out.println("After MatchAllFilter read of " + matchFilters.size());        
    }
    
    public void testMatchAllFilter2(){
        EarthQuakeParser parser = new EarthQuakeParser(); 
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        String source = "data/nov20quakedatasmall.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);         
        System.out.println("read data for "+list.size()+" quakes");
        /*   
        for (QuakeEntry qe: list) { 
            System.out.println(qe);
        }
        */
        System.out.println("read of " + list.size());
        
        MatchAllFilter maf = new MatchAllFilter();
        
        maf.AddFilter(new MagnitudeFilter(0.0, 3.0));
        maf.AddFilter(new DistanceFilter(new Location(36.1314, -95.9372), 10000));
        maf.AddFilter(new PhraseFilter("any", "Ca"));
        
        ArrayList<QuakeEntry> matchFilters = filter(list, maf);
        
        for (QuakeEntry qe: matchFilters) { 
            System.out.println(qe);
        }
        
        System.out.println("Filters used are: " + maf.getName());
        System.out.println("After MatchAllFilter read of " + matchFilters.size());        
    }    
    
    public void testMatchAllFilter3(){
        EarthQuakeParser parser = new EarthQuakeParser(); 
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        String source = "data/nov20quakedata.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);         
        System.out.println("read data for "+list.size()+" quakes");
        /*   
        for (QuakeEntry qe: list) { 
            System.out.println(qe);
        }
        */
        System.out.println("read of " + list.size());
        
        MatchAllFilter maf = new MatchAllFilter();

        maf.AddFilter(new MagnitudeFilter(0.0, 5.0));
        maf.AddFilter(new DistanceFilter(new Location(55.7308, 9.1153), 3000));        
        maf.AddFilter(new PhraseFilter("any", "e"));
        //maf.AddFilter(new DepthFilter(-180000.0, -30000.0));        
        
        ArrayList<QuakeEntry> matchFilters = filter(list, maf);
        
        for (QuakeEntry qe: matchFilters) { 
            System.out.println(qe);
        }
        
        System.out.println("Filters used are: " + maf.getName());
        System.out.println("After MatchAllFilter read of " + matchFilters.size());        
    }        
}
