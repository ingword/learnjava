import java.util.*;

/**
 * Write a description of class MatchAllFilter here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MatchAllFilter implements Filter{

    private ArrayList<Filter> filters;
    
    public MatchAllFilter(){
        filters = new ArrayList<Filter>();
    }
    
    public  boolean satisfies(QuakeEntry qe){
        for (Filter f : filters){
            if (!f.satisfies(qe)){
                return false;
            }
        }
        return true;
    }
    
    public void AddFilter(Filter filter){
        filters.add(filter);
    }
    
    public String getName(){
        String result = "";
        for (Filter f : filters){
            result = result + f.getName() + " ";
        }
        return result;
    }    
}
