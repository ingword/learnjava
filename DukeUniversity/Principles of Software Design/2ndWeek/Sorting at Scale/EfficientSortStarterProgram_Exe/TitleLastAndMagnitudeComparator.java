
/**
 * Write a description of class TitleLastAndMagnitudeComparator here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.*;
public class TitleLastAndMagnitudeComparator implements Comparator <QuakeEntry> {
    private String lastWord(String str){
        return str.substring(str.lastIndexOf(" ")+1);
    }
    
    public int compare(QuakeEntry qe1, QuakeEntry qe2) {
        String title1LastWord = lastWord(qe1.getInfo());
        String title2LastWord = lastWord(qe2.getInfo());
        
        if (title1LastWord.compareTo(title2LastWord) < 0)
            return -1;
            
        if (title1LastWord.compareTo(title2LastWord) > 0)
            return 1;

        return Double.compare(qe1.getMagnitude(), qe2.getMagnitude());
    }
}