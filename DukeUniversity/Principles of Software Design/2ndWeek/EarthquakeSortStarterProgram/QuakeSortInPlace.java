
/**
 * Write a description of class QuakeSortInPlace here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

import java.util.*;
import edu.duke.*;

public class QuakeSortInPlace
{
    private void sortByLargestDepth(ArrayList<QuakeEntry> quakeData){
       for (int i=0; i < quakeData.size(); i++) {
            int maxIdx = getLargestDepth(quakeData,i);
            QuakeEntry qi = quakeData.get(i);
            QuakeEntry qmax = quakeData.get(maxIdx);
            quakeData.set(i,qmax);
            quakeData.set(maxIdx,qi);
            //if (i==50) break;            
        }        
    }
    
    public void sortByMagnitudeWithCheck(ArrayList<QuakeEntry> quakeData){
        for (int i=0; i< quakeData.size(); i++) {
            int minIdx = getSmallestMagnitude(quakeData,i);
            QuakeEntry qi = quakeData.get(i);
            QuakeEntry qmin = quakeData.get(minIdx);
            quakeData.set(i,qmin);
            quakeData.set(minIdx,qi);
            System.out.println("----------------------------");             
            System.out.println(i + " passes");            
            if (checkInSortedOrder(quakeData)) 
                break;
        }        
    }
    
    public void sortByMagnitudeWithBubbleSortWithCheck(ArrayList<QuakeEntry> quakeData){
        //for (int i=0; i < quakeData.size() - 1; i++){
            onePassBubbleSort(quakeData, quakeData.size() - 1);
            //System.out.println(i + " passes");            
            //if (checkInSortedOrder(quakeData)) 
            //    break;
        //}

    }
    
    private boolean checkInSortedOrder(ArrayList<QuakeEntry> quakeData){
        boolean result = true;
        for (int i=0; i < quakeData.size() - 1; i++){
            if (result && quakeData.get(i+1).getMagnitude() > quakeData.get(i).getMagnitude())
               result = true;
            else
               result = false;
            }
        return result;
    }
    
    private void writeArray(ArrayList<QuakeEntry> quakeData){
        for (QuakeEntry qe: quakeData) { 
            System.out.println(qe);
        }         
    }
    
    public void onePassBubbleSort(ArrayList<QuakeEntry> quakeData, int numSorted){
        for (int i = 0; i < numSorted; i++){
            for (int j = 0; j < quakeData.size() - 1; j++)
                if (quakeData.get(j).getMagnitude() > quakeData.get(j+1).getMagnitude()){
                    QuakeEntry qe = quakeData.get(j);
                    quakeData.set(j, quakeData.get(j+1));
                    quakeData.set(j+1, qe);
                }
            System.out.println("After " + i + " pass");
            if (checkInSortedOrder(quakeData)) break;
            //writeArray(quakeData);
            }
    }
    
    public void sortByMagnitudeWithBubbleSort(ArrayList<QuakeEntry> quakeData){
        onePassBubbleSort(quakeData, quakeData.size() - 1);
    }
    
    public int getLargestDepth (ArrayList<QuakeEntry> quakes, int from){
        int maxIndex = from;
        for (int i = from + 1; i < quakes.size(); i++) {
            if (quakes.get(i).getDepth() > quakes.get(maxIndex).getDepth()) {
                maxIndex = i;
            }
        }
        return maxIndex;
    }
    
    public void sortByDepth(ArrayList<QuakeEntry> in) {
       
       for (int i=0; i< in.size(); i++) {
            int minIdx = getLargestDepth(in,i);
            QuakeEntry qi = in.get(i);
            QuakeEntry qmin = in.get(minIdx);
            in.set(i,qmin);
            in.set(minIdx,qi);
        }
        
    }    
    
    public QuakeSortInPlace() {
        // TODO Auto-generated constructor stub
    }
   
    public int getSmallestMagnitude(ArrayList<QuakeEntry> quakes, int from) {
        int minIdx = from;
        for (int i=from+1; i< quakes.size(); i++) {
            if (quakes.get(i).getMagnitude() < quakes.get(minIdx).getMagnitude()) {
                minIdx = i;
            }
        }
        return minIdx;
    }
    
    public void sortByMagnitude(ArrayList<QuakeEntry> in) {
       for (int i=0; i< in.size(); i++) {
            int minIdx = getSmallestMagnitude(in,i);
            QuakeEntry qi = in.get(i);
            QuakeEntry qmin = in.get(minIdx);
            in.set(i,qmin);
            in.set(minIdx,qi);
        }
        
    }

    public void testSort() {
        EarthQuakeParser parser = new EarthQuakeParser(); 
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        //String source = "data/nov20quakedatasmall.atom";
        //String source = "data/nov20quakedata.atom";
        String source = "data/earthQuakeDataWeekDec6sample1.atom";
        //String source = "data/earthquakeDataSampleSix2.atom";        
        ArrayList<QuakeEntry> list  = parser.read(source);  
       
        System.out.println("read data for "+list.size()+" quakes");    
        
        //sortByMagnitude(list);
        sortByLargestDepth(list);
        //sortByMagnitudeWithBubbleSort(list);
        //sortByMagnitudeWithBubbleSortWithCheck(list);
        //sortByMagnitudeWithCheck(list);
       
        //System.out.println(isSortedArray);

        writeArray(list);
    }
    
    public void createCSV(){
        EarthQuakeParser parser = new EarthQuakeParser();
        String source = "data/earthQuakeDataDec6sample2.atom";
        //String source = "data/nov20quakedatasmall.atom";
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        dumpCSV(list);
        System.out.println("# quakes read: "+list.size());
    }
    
    public void dumpCSV(ArrayList<QuakeEntry> list){
        System.out.println("Latitude,Longitude,Magnitude,Info");
        for(QuakeEntry qe : list){
            System.out.printf("%4.2f,%4.2f,%4.2f,%s\n",
                              qe.getLocation().getLatitude(),
                              qe.getLocation().getLongitude(),
                              qe.getMagnitude(),
                              qe.getInfo());
        }
        
    }
}
