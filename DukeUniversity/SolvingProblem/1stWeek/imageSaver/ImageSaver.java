/**
 * Make copies of all images selected within a directory (or folder).
 * 
 * @author Duke Software Team 
 */
import edu.duke.*;
import java.io.File;

public class ImageSaver {
    public ImageResource makeInversion(ImageResource inImage) {
        //I made a blank image of the same size
        ImageResource outImage = new ImageResource(inImage.getWidth(), inImage.getHeight());
        //for each pixel in outImage
        for (Pixel pixel: outImage.pixels()) {
            //look at the corresponding pixel in inImage
            Pixel inPixel = inImage.getPixel(pixel.getX(), pixel.getY());
            //compute inPixel's red + inPixel's blue + inPixel's green
            //divide that sum by 3 (call it average)
            
            int outRed = 255 - inPixel.getRed();
            int outBlue = 255 - inPixel.getBlue();
            int outGreen = 255 - inPixel.getGreen();            
            //set pixel's red to average
            pixel.setRed(outRed);
            //set pixel's green to average
            pixel.setGreen(outGreen);
            //set pixel's blue to average
            pixel.setBlue(outBlue);
        }
        //outImage is your answer
        return outImage;
    }
    
    //I started with the image I wanted (inImage)
    public ImageResource makeGray(ImageResource inImage) {
        //I made a blank image of the same size
        ImageResource outImage = new ImageResource(inImage.getWidth(), inImage.getHeight());
        //for each pixel in outImage
        for (Pixel pixel: outImage.pixels()) {
            //look at the corresponding pixel in inImage
            Pixel inPixel = inImage.getPixel(pixel.getX(), pixel.getY());
            //compute inPixel's red + inPixel's blue + inPixel's green
            //divide that sum by 3 (call it average)
            int average = (inPixel.getRed() + inPixel.getBlue() + inPixel.getGreen())/3;
            //set pixel's red to average
            pixel.setRed(average);
            //set pixel's green to average
            pixel.setGreen(average);
            //set pixel's blue to average
            pixel.setBlue(average);
        }
        //outImage is your answer
        return outImage;
    }

    public void selectAndConvert () {
        DirectoryResource dr = new DirectoryResource();
        for (File f : dr.selectedFiles()) {
            ImageResource inImage = new ImageResource(f);
            ImageResource gray = makeGray(inImage);
            gray.draw();
        }
    }
    
    public void selectAndInvert () {
        DirectoryResource dr = new DirectoryResource();
        for (File f : dr.selectedFiles()) {
            ImageResource inImage = new ImageResource(f);
            ImageResource invert = makeInversion(inImage);
            invert.draw();
        }
    }
    
    public void doGrayScaleAndSave() {      
        DirectoryResource dr = new DirectoryResource();
        //Your program should let the user select multiple image files
        for (File f : dr.selectedFiles()) {
            ImageResource image = new ImageResource(f);
            //For each image, create a new image that is a grayscale version of the original image
            ImageResource gray = makeGray(image);
            
            String fname = image.getFileName();
            String newName = "gray-" + fname;
            gray.setFileName(newName);
            //image.draw();            
            gray.save();
        }
    }
    
    public void doInvertAndSave() {      
        DirectoryResource dr = new DirectoryResource();
        //Your program should let the user select multiple image files
        for (File f : dr.selectedFiles()) {
            ImageResource image = new ImageResource(f);
            //For each image, create a new image that is a grayscale version of the original image
            ImageResource invert = makeInversion(image);
            
            String fname = image.getFileName();
            String newName = "invert-" + fname;
            invert.setFileName(newName);
            invert.draw();            
            invert.save();
        }
    }    
}
